# README

This is an example of load balancing with a reverse proxy, using Docker, NGINX, and simple HTML.

## App Information

App Name: load-balancing

Created: May 2018

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/load-balancing)

## Notes

* The reverse proxy runs on port 80.

* The NGINX server distributes requests to one of five sites.

* Some sites are weighted higher and receive more loads than others.

## To Run

```
$ source build.sh
```

Last updated: 2025-01-06
